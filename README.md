# dcc053_compilers_I

## Authors/Autores:  
Eduardo  
https://gitlab.com/laura-vianna

### Repository for group coursework of Compilers I module - 2017/2  

1. tp1 - LALR Lexical analyzer and parser - FLEX, BISON, C++  
2. tp2 - A compiler's front-end for SmallL language, genarating intermediate code in the process - FLEX, BISON, C++  
3. tp3 - Translator from intermediate code to MEPA interpreter format - FLEX, BISON, C++  
4. tp4 - Unified and integrated compiler - FLEX, BISON, C++, Pyhton3  


### Repositorio para trabalho em grupo da disciplina Compiladores I - 2017/2 

1. tp1 - Analisador léxico e o analisador sintático LALR - FLEX, BISON, C++  
2. tp2 - Front-end de um compilador para a linguagem SmallL, gerando codigo intermediario - FLEX, BISON, C++  
3. tp3 - Tradutor de codigo intermediario para o interpretador MEPA - FLEX, BISON, C++  
4. tp4 - Compilador unificado e integrado - FLEX, BISON, C++, Pyhton3  
