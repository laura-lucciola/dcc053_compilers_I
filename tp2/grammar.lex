%option noyywrap
%option yylineno

/*necessary C libraries, Global methods and variables*/
%{
#include <iostream>
#include <string>
#include "symbol_table.h"

using namespace std;

/*#define TEST_MODE 1*/

#ifdef TEST_MODE

    #define	TOKEN(t) cout << "Token: " << #t << endl;
    #define ID_TOKEN(t) cout << "ID Token: "<< yytext << " Token: " << #t << endl; install_symbol(yytext, #t);
    #define	FLOAT_TOKEN(t) cout << "Float " << atof(yytext) << " Token: " << #t << endl;
    #define INTEGER_TOKEN(t) cout << "Integer "<< atoi(yytext) << " Token: " << #t << endl;
    #define UNKOWN_TOKEN(t) cout << "L_ERROR: Unexpected symbol in lexical analyser: " << yytext << endl;

#else
    #include "parser.tab.h"

    #define	TOKEN(t); return (t);
    #define	ID_TOKEN(t) yylval.str = yytext; install_symbol(yytext, #t); return(t);
    #define	FLOAT_TOKEN(t) yylval.real_num = atof(yytext); return(t);
    #define	INTEGER_TOKEN(t) yylval.int_num = atoi(yytext); return(t);
    #define UNKOWN_TOKEN(t) cerr << "L_ERROR: Unexpected symbol in lexical analyser: " << yytext << endl;

    void yyerror(char *s);

#endif
%}
/*definitions*/
delimitator     [ \t\r]
end_of          {delimitator}+
digit           [0-9]
letter       	[A-Za-z]
id              {letter}+
number          {digit}+
integer_num 	{number}
real_num		{number}\.{number}

%%

";"             TOKEN(SEMICOLON);
"="             TOKEN(EQUALS);
"||"            TOKEN(LOGICAL_OR);
"&&"            TOKEN(LOGICAL_AND);
"=="            TOKEN(IS_EQUAL_TO);
"!="            TOKEN(IS_NOT_EQUAL_TO);
"<"             TOKEN(LESS_THAN);
">"             TOKEN(GREATER_THAN);
"<="            TOKEN(LESS_EQUAL_THAN);
">="            TOKEN(GREATER_EQUAL_THAN);
"+"             TOKEN(MATH_PLUS);
"-"             TOKEN(MATH_MINUS);
"*"             TOKEN(MATH_TIMES);
"/"             TOKEN(MATH_DIVIDE);
"!"             TOKEN(LOGICAL_NOT);
"["             TOKEN(BRACKET_BEGIN);
"]"             TOKEN(BRACKET_END);
"("             TOKEN(PARENTHESES_BEGIN);
")"             TOKEN(PARENTHESES_END);
"{"             TOKEN(BLOCK_BEGIN);
"}"             TOKEN(BLOCK_END);
int             TOKEN(INTEGER_TYPE);
float           TOKEN(REAL_TYPE);
bool            TOKEN(BOOLEAN_TYPE);
true            TOKEN(BOOLEAN_TRUE);
false           TOKEN(BOOLEAN_FALSE);
if              TOKEN(IF);
else            TOKEN(ELSE);
while           TOKEN(WHILE);
do              TOKEN(DO);
break           TOKEN(BREAK);
{real_num}		FLOAT_TOKEN(FLOAT_NUM);
{integer_num}   INTEGER_TOKEN(INTEGER_NUM);
{id}            ID_TOKEN(ID_VARIABLE);
{end_of}        ;
\n		        {  }
.               UNKOWN_TOKEN(.);

%%

void open_file(int argc, const char *filename){
    if(argc < 2){
        cerr << "I_ERROR: Wrong number os arguments! Please specify a file name"<< endl;
        exit(1);
    }
    if ((freopen(filename, "r", stdin) == NULL)) {
        cerr << "I_ERROR: File " << filename << " cannot be opened" << endl;
        exit(1);
    }
}

#ifdef TEST_MODE

    int main(int argc, char **argv) {
        open_file(argc, argv[1]);

        init_symbol_table(); // Initialize symbol table

        yylex();

        print_symbol_table();

        return 0;
    }

#else

    int main(int argc, char **argv) {
        open_file(argc, argv[1]);

        init_symbol_table(); // Initialize symbol table

        yyparse();

        return 0;
    }

#endif
