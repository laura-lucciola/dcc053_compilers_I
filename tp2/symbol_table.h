#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

    #include <iostream>
    #include <string.h>
    using namespace std;

    #define NMax 100     /* Numero maximo de niveis possiveis */
    #define name_max 20

    /******************************************************************************
    Function Headers
    (Este arquivo foi baseado nas funcoes da lista nao ordenada fornecidas no site)
    https://homepages.dcc.ufmg.br/~mariza/Cursos/CompiladoresI/Geral/CodigoTS/codigoTS-C.html
    ******************************************************************************/

    void init_symbol_table(void);

    int get_entry(char name[name_max]);

    void install_symbol(char name[name_max], char atributo[name_max]);

    void print_symbol_table();

#endif
