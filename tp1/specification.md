Last Updated on 23/08/17

Universidade Federal de Minas Gerais
Departamento de Ciência da Computação
COMPILADORES I
Professora: Mariza Andrade da Silva Bigonha
TRABALHO PRÁTICO 1 - INDIVIDUAL
8 PONTOS
13/09/2017
1

*Problema

Para a gramática a seguir, implemente o analisador léxico e o analisador sintático LALR. O
trabalho pode ser feito em Java ou C++, portanto use as ferramentas adequadas à linguagem
escolhida. O seu trabalho recebe como entrada programas fontes, por exemplo, testeTP1, testeTP2, etc. Faz as análises léxica e sintática e produz como resultado as produções usadas
durante a análise sintática e uma mensagem dizendo se os testes submetidos ao front-end do
compilador estão sintáticamente corretos.


*Project

For the following grammar, implement the lexical analyzer and the LALR parser. Work can be done in Java or C ++, so use the tools appropriate to the language Chosen. Your work receives incoming source programs, for example, test1, test2, etc. It does the lexical and syntactic analysis and produces as a result the productions used During the parsing and a message stating whether the tests submitted to the front end of the Compiler are syntactically correct.
