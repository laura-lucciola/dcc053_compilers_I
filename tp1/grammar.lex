%option noyywrap
%option yylineno

/*necessary C libraries, Global methods and variables*/
%{
#include <iostream>
using namespace std;

/*#define TEST_MODE 1*/

#ifdef TEST_MODE

    #define	TOKEN(t) cout << "Token: " << #t << endl;
    #define ID_TOKEN(t) cout << "ID Token: "<< yytext << " Token: " << #t << endl;
    #define	FLOAT_TOKEN(t) cout << "Float " << atof(yytext) << " Token: " << #t << endl ;
    #define INTEGER_TOKEN(t) cout << "Integer "<< atoi(yytext) << " Token: " << #t << endl;
    #define UNKOWN_TOKEN(t) cout << "L_ERROR: Unexpected symbol in lexical analyser: " << yytext << endl;

#else
    #include "parser.tab.h"

    #define	TOKEN(t) return (t);
    #define	ID_TOKEN(t) yylval.str = yytext; return(t);
    #define	FLOAT_TOKEN(t) yylval.real_num = atof(yytext); return(t);
    #define	INTEGER_TOKEN(t) yylval.int_num = atoi(yytext); return(t);
    #define UNKOWN_TOKEN(t) cerr << "L_ERROR: Unexpected symbol in lexical analyser: " << yytext << endl;

    void yyerror(char *s);

#endif
%}
/*definitions*/
delimitator     [ \t\r]
end_of          {delimitator}+
digit           [0-9]
letter       	[A-Za-z]
id              {letter}+
number          {digit}+
integer_num 	{number}
real_num		{number}\.{number}

%%

";"             TOKEN(SEMICOLON);
"="             TOKEN(EQUALS);
"<"             TOKEN(LESSTHAN);
">"             TOKEN(GREATERTHAN);
"<="            TOKEN(LESSEQUALTHAN);
">="            TOKEN(GREATEREQUALTHAN);
"+"             TOKEN(PLUS);
"-"             TOKEN(MINUS);
"*"             TOKEN(TIMES);
"/"             TOKEN(DIVIDE);
"("             TOKEN(BRACKET_BEGIN);
")"             TOKEN(BRACKET_END);
"{"             TOKEN(BLOCK_BEGIN);
"}"             TOKEN(BLOCK_END);
int             TOKEN(INTEGER_TYPE);
char            TOKEN(CHARACTER_TYPE);
bool            TOKEN(BOOLEAN_TYPE);
float           TOKEN(REAL_TYPE);
if              TOKEN(IF);
else            TOKEN(ELSE);
while           TOKEN(WHILE);
{real_num}		FLOAT_TOKEN(FLOAT_NUM);
{integer_num}   INTEGER_TOKEN(INTEGER_NUM);
{id}            ID_TOKEN(ID_VARIABLE);
{end_of}        ;
\n		        {  }
.               UNKOWN_TOKEN(.);

%%

void open_file(int argc, const char *filename){
    if(argc < 2){
        cerr << "I_ERROR: Wrong number os arguments! Please specify a file name"<< endl;
        exit(1);
    }
    if ((freopen(filename, "r", stdin) == NULL)) {
        cerr << "I_ERROR: File " << filename << " cannot be opened" << endl;
        exit(1);
    }
}

#ifdef TEST_MODE

    int main(int argc, char **argv) {
        open_file(argc, argv[1]);
        yylex();
        return 0;
    }

#else

    int main(int argc, char **argv) {
        open_file(argc, argv[1]);
        yyparse();
        return 0;
    }

#endif
