%{

#include <iostream>
#include <string>
using namespace std;

void yyerror(char *s);
int yylex(void);
%}

%union {
  int int_num;
  float real_num;
  char* str;
};

%token SEMICOLON EQUALS LESSTHAN GREATERTHAN LESSEQUALTHAN GREATEREQUALTHAN
%token PLUS MINUS TIMES DIVIDE IF ELSE WHILE
%token BRACKET_BEGIN BRACKET_END BLOCK_BEGIN BLOCK_END
%token INTEGER_TYPE CHARACTER_TYPE BOOLEAN_TYPE REAL_TYPE ID_VARIABLE

%token <int_num> INTEGER_NUM
%token <real_num> FLOAT_NUM

%nonassoc NO_ELSE
%nonassoc ELSE

%%
program: block { cout << "P_SUCCESS: File was parsed to the end" << endl;}
    | block error
  ;

block:
    BLOCK_BEGIN decls stmts BLOCK_END
  ;

decls:
  | decls decl
  ;

decl:
    type ID_VARIABLE SEMICOLON
  ;

type:
    INTEGER_TYPE
  | CHARACTER_TYPE
  | BOOLEAN_TYPE
  | REAL_TYPE
  ;

stmts:
  | stmts stmt
  ;

stmt:
    ID_VARIABLE EQUALS expr SEMICOLON
  | IF BRACKET_BEGIN rel BRACKET_END stmt %prec NO_ELSE
  | IF BRACKET_BEGIN rel BRACKET_END stmt ELSE stmt
  | WHILE BRACKET_BEGIN rel BRACKET_END stmt
  | block
  ;

rel:
    expr LESSTHAN expr
  | expr LESSEQUALTHAN expr
  | expr GREATEREQUALTHAN expr
  | expr GREATERTHAN expr
  | expr
  ;

expr:
    expr PLUS term
  | expr MINUS term
  | term
  ;

term:
    term TIMES unary
  | term DIVIDE unary
  | unary
  ;

unary:
    MINUS unary
  | factor
  ;

factor:
    INTEGER_NUM
  | FLOAT_NUM
  ;

%%
void yyerror(string s) {
  extern int yylineno;	// defined in %option yylineno at flex file
  extern char *yytext;	// defined and maintained at flex file

  cerr << "P_ERROR: " << s << " at symbol \"" << yytext;
  cerr << "\" on line " << yylineno << endl;
}

void yyerror(char *s) {
  return yyerror(string(s));
}
