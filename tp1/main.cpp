#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <stdio.h>

#define DEBUG 1
#define CHAR_SIZE 15

using namespace std;

void debug(char * message) {
    if (DEBUG)
    cout << "[debug]" << message << '\n';
}

void debug(char * message, const char * object) {
    if (DEBUG)
    cout << "[debug]" << message << ' ' << object << '\n';
}

void readFile(const char *filename)
{
    ifstream source;
    stringstream buffer;
    char bufferChar, str[CHAR_SIZE];

    source.open(filename);
    if (source.is_open())
    debug("File is open", filename);
    else
    debug("Error when opening", filename);

    buffer << source.rdbuf();

    source.close();
    debug("File is closed", filename);

    while(buffer >> bufferChar){
        str[0] = bufferChar;
        debug("Element is: ", str);
    }
}


int main(int argc, char const *argv[]) {
    if(argc != 2){
        debug("Wrong number os arguments!");
    }
    const char * filename(argv[1]);

    readFile(filename);

    return 0;
}
