#include <stdio.h>
#include <stdlib.h>

#include "translator.h"

int main(int argc, char* argv[]) {
	// Checa o numero de argumentos
	if(argc != 4) {
		printf("Erro, numero incorreto de parametros");
		return 0;
	}

	char temp[500];

	// Chama  analisador lexico e sintatico
	sprintf(temp, "./parser.out %s > %s", argv[1], argv[2]);
	system(temp);

	// Chama o tradutor
	translate_file(argv[2], argv[3]);

	// Chama a interpretador
	sprintf(temp, "python mepa_interpreter.py %s", argv[3]);
	system(temp);

	return 0;
}
