#include "translator.h"

int translate_file(char* in_file, char* out_file) {

	// Arquivos
	FILE* f_in;
	FILE* f_out;
	f_in = fopen(in_file, "r");
	f_out = fopen(out_file, "w");

	// Verifica se o arquivo foi lido
	if ((f_in == NULL) || (f_out == NULL)) {
		printf("Erro ao abrir arquivo(s)");
		return 0;
	}

	// Le o arquivo de entrada
	char* line = NULL;
	size_t len = 0;
    ssize_t read;

    // Inicializa a posicao dos enderecos
 	end_len = 0;
 	offset = 0;

    fprintf(f_out, "INPP\n");

    int var_space;
    var_space = get_var_space(f_in);
    fprintf(f_out, "AMEM %d\n", var_space);
    fclose(f_in);
	f_in = fopen(in_file, "r");

    // Traduz o codigo linha por linha
	while ((read = getline(&line, &len, f_in)) != -1) {
        translate_line(f_out, line);
    }

	fprintf(f_out, "PARA\n");

    fclose(f_in);
    fclose(f_out);

	return 0;
}


void translate_line(FILE* f_out, char* line) {
	char* ch;

	// Remove \n no final se houver
	if(line[strlen(line)-1] == '\n')
		line[strlen(line)-1] = '\0';

	// Se for uma linha vazia
	if(strlen(line) == 0)
		return;

	// Le o primeiro token
	ch = strtok(line, " ");

	// Enquanto for um rotulo
	while(ch[strlen(ch)-1] == ':') {
		fprintf(f_out, "%s ", ch);
		ch = strtok(NULL, " ");
		if(ch == NULL) {
			fprintf(f_out, "\n");
			return;
		}
	}

	// Goto
	if(strcmp(ch, "goto") == 0) {
		ch = strtok(NULL, " ");
		fprintf(f_out, "DSVS %s\n", ch);
		return;
	}

	// Iffalse
	if(strcmp(ch, "iffalse") == 0) {
		char* cond;

		// Condicao do if
		cond = strtok(NULL, " ");
		imprime_operando(f_out, cond);

		// Pula o goto
		strtok(NULL, " ");

		// Destino do goto
		ch = strtok(NULL, " ");

		fprintf(f_out, "DSVF %s \n", ch);
		return;
	}

	// If
	if(strcmp(ch, "if") == 0) {
		char* cond;

		// Condicao do if
		cond = strtok(NULL, " ");
		imprime_operando(f_out, cond);

		// Nega a condicao
		fprintf(f_out, "NEGA\n");

		// Pula o goto
		strtok(NULL, " ");

		// Destino do goto
		ch = strtok(NULL, " ");

		fprintf(f_out, "DSVF %s \n", ch);
		return;
	}

	// Atribuicoes
	char *result, *fst_op, *snd_op, *op;
	result = ch;

	// Pula o =
	strtok(NULL, " ");

	// Primeiro operando
	fst_op = strtok(NULL, " ");
	imprime_operando(f_out, fst_op);

	// Operador
	op = strtok(NULL, " ");

	// Se o operando for nulo, faz apenas uma atribuicao direta
	if(op == NULL) {
		print_result(f_out, result);
		return;
	}

	// Se tiver segundo operando
	snd_op = strtok(NULL, " ");
	if(snd_op != NULL) {
		imprime_operando(f_out, snd_op);
		print_operator2(f_out, op);

	} else { // Se nao imprime o operador unario
		print_operator1(f_out, op);
	}

	// Salva resultado
	print_result(f_out, result);
}

int id_pos(FILE* f_out, char* id) {
	int i, j;

	// Se nao for array
	if(is_array(id) == 0) {
		// Busca entrada
		for(i=0; i<end_len; i++) {
			if(strcmp(enderecos[i].name, id) == 0) {
				return enderecos[i].offset;
			}
		}

		// Erro que nao deveria acontecer
		printf("Erro, variavel nao encontrada");
		return -1;
	}

	char arr[100], ind[100];

	// Separa o nome do array e o indice
	i = 0;
	while(id[i] != '[') {
		arr[i] = id[i];
		i += 1;
	}
	arr[i] = '\0';

	i += 1;
	j = 0;
	while(id[i] != ']') {
		ind[j] = id[i];
		j += 1;
		i += 1;
	}
	ind[j] = '\0';

	// Busca entrada
	for(i=0; i<end_len; i++) {
		if(strcmp(enderecos[i].name, arr) == 0) {
			return enderecos[i].offset;
		}
	}

	// Erro que nao deveria acontecer
	printf("Erro, variavel nao encontrada");
	return -1;
}

int is_array(char* ch) {
	if(ch[strlen(ch)-1] == ']')
		return 1;
	else
		return 0;
}


int is_number(char* ch) {
	int i;
	for(i=0; i<strlen(ch); i++) {
		if(isdigit(ch[i]) == 0 && ch[i] != '.') {
			return 0;
		}
	}

	return 1;
}

void print_result(FILE* f_out, char* op) {
	int pos, i, j;
	pos = id_pos(f_out, op);

	// Verifica se eh arranjo
	if(is_array(op) == 0)
		fprintf(f_out, "ARMZ 0 %d\n", pos);
	else {
		char arr[100], ind[100];

		// Separa o nome do array e o indice
		i = 0;
		while(op[i] != '[') {
			arr[i] = op[i];
			i += 1;
		}
		arr[i] = '\0';

		i += 1;
		j = 0;
		while(op[i] != ']') {
			ind[j] = op[i];
			j += 1;
			i += 1;
		}
		ind[j] = '\0';

		// Carrega o indice na pilha
		imprime_operando(f_out, ind);

		fprintf(f_out, "ARMP 0 %d\n", pos);
	}
}

void imprime_operando(FILE* f_out, char* op) {
	// Constante false
	if(strcmp(op, "false") == 0) {
		fprintf(f_out, "CRCT 0\n");
		return;
	}

	// Constante true
	if(strcmp(op, "true") == 0) {
		fprintf(f_out, "CRCT 1\n");
		return;
	}

	// Constante numerica
	if(is_number(op)) {
		fprintf(f_out, "CRCT %s\n", op);
		return;
	}

	// Ids
	int pos, i, j;
	pos = id_pos(f_out, op);
	if(is_array(op) == 0)
		fprintf(f_out, "CRVL 0 %d\n", pos);
	else {
		char arr[100], ind[100];

		// Separa o nome do array e o indice
		i = 0;
		while(op[i] != '[') {
			arr[i] = op[i];
			i += 1;
		}
		arr[i] = '\0';

		i += 1;
		j = 0;
		while(op[i] != ']') {
			ind[j] = op[i];
			j += 1;
			i += 1;
		}
		ind[j] = '\0';

		// Carrega o indice na pilha
		imprime_operando(f_out, ind);

		fprintf(f_out, "CRVP 0 %d\n", pos);
	}
}

void print_operator1(FILE* f_out, char* op) {
	if(strcmp(op, "-") == 0) {
		fprintf(f_out, "INVR\n");
		return;
	}

	if(strcmp(op, "!") == 0) {
		fprintf(f_out, "NEGA\n");
		return;
	}
}

void print_operator2(FILE* f_out, char* op) {
	if(strcmp(op, "+") == 0) {
		fprintf(f_out, "SOMA\n");
		return;
	}

	if(strcmp(op, "-") == 0) {
		fprintf(f_out, "SUBT\n");
		return;
	}

	if(strcmp(op, "*") == 0) {
		fprintf(f_out, "MULT\n");
		return;
	}

	if(strcmp(op, "/") == 0) {
		fprintf(f_out, "DIVI\n");
		return;
	}

	if(strcmp(op, ">") == 0) {
		fprintf(f_out, "CMMA\n");
		return;
	}

	if(strcmp(op, "<") == 0) {
		fprintf(f_out, "CMME\n");
		return;
	}

	if(strcmp(op, "<=") == 0) {
		fprintf(f_out, "CMEG\n");
		return;
	}

	if(strcmp(op, ">=") == 0) {
		fprintf(f_out, "CMAG\n");
		return;
	}

	if(strcmp(op, "==") == 0) {
		fprintf(f_out, "CMIG\n");
		return;
	}

	if(strcmp(op, "!=") == 0) {
		fprintf(f_out, "CMDG\n");
		return;
	}

	if(strcmp(op, "||") == 0) {
		fprintf(f_out, "DISJ\n");
		return;
	}

	if(strcmp(op, "&&") == 0) {
		fprintf(f_out, "CONJ\n");
		return;
	}
}


int id_size(char* id) {
	int i, j;

	// Constante false
	if(strcmp(id, "false") == 0)
		return 0;

	// Constante true
	if(strcmp(id, "true") == 0)
		return 0;

	// Constante numerica
	if(is_number(id))
		return 0;

	// Se nao for array
	if(is_array(id) == 0) {
		// Se a entrada ja existir
		for(i=0; i<end_len; i++) {
			if(strcmp(enderecos[i].name, id) == 0) {
				return 0;
			}
		}

		// Senao cria uma nova
		enderecos[i].offset = offset;
		strcpy(enderecos[i].name, id);

		offset += 1;
		end_len += 1;

		return 1;
	}

	// Se for array
	char arr[100], ind[100];

	// Separa o nome do array e o indice
	i = 0;
	while(id[i] != '[') {
		arr[i] = id[i];
		i += 1;
	}
	arr[i] = '\0';

	i += 1;
	j = 0;
	while(id[i] != ']') {
		ind[j] = id[i];
		j += 1;
		i += 1;
	}
	ind[j] = '\0';

	// Se a entrada ja existir
	for(i=0; i<end_len; i++) {
		if(strcmp(enderecos[i].name, arr) == 0) {
			return 0;
		}
	}

	// Senao cria uma nova
	enderecos[i].offset = offset;
	strcpy(enderecos[i].name, arr);

	offset += ARRAY_SIZE;
	end_len += 1;

	return ARRAY_SIZE;
}

int get_var_space(FILE* f_in) {
	int n_mem = 0;
	char* line = NULL;
	char buffer[100];
	size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, f_in)) != -1) {
        char* ch;
        char* cond;

		// Remove \n no final se houver
		if(line[strlen(line)-1] == '\n')
			line[strlen(line)-1] = '\0';

		// Se for uma linha vazia
		if(strlen(line) == 0)
			continue;

		// Le o primeiro token
		strcpy(buffer, line);
		ch = strtok(buffer, " ");

		// Enquanto for um rotulo
		while(ch[strlen(ch)-1] == ':') {
			ch = strtok(NULL, " ");
			if(ch == NULL)
				break;
		}

		if(ch == NULL)
			continue;

		if(strcmp(ch, "goto") == 0)
			continue;

		// Iffalse
		if(strcmp(ch, "iffalse") == 0) {
			cond = strtok(NULL, " ");
			n_mem += id_size(cond);
			continue;
		}

		// If
		if(strcmp(ch, "if") == 0) {
			cond = strtok(NULL, " ");
			n_mem += id_size(cond);
			continue;
		}

		n_mem += id_size(ch);

		// Pula o =
		strtok(NULL, " ");

		// Primeiro operando
		ch = strtok(NULL, " ");
		n_mem += id_size(ch);

		ch = strtok(NULL, " ");
		if(ch == NULL)
			continue;

		// Se tiver segundo operando
		ch = strtok(NULL, " ");
		if(ch != NULL)
			n_mem += id_size(ch);

    }

    return n_mem;
}
