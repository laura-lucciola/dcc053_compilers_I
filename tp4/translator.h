#ifndef TRANSLATOR
#define TRANSLATOR

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ARRAY_SIZE 100

// Estrutura que salva a posicao dos nomes na memoria
typedef struct {
	char name[100];
	int offset;
} id;

id enderecos[100];
int end_len;
int offset;

// Traduz o arquivo de entrada e salva no arquivo de saida
int translate_file(char* in_file, char* out_file);

// Traduz uma linha de codigo intermediario
void translate_line(FILE* f_out, char* line);

// Verifica se o operando eh um numero
int is_number(char* ch);

// Imprime o codigo que empilha o operando
void imprime_operando(FILE* f_out, char* op);

// Imprime o codigo de um operador unario
void print_operator1(FILE* f_out, char* op);

// Imprime o codigo de um operador bianrio
void print_operator2(FILE* f_out, char* op);

// Imprime o codigo que salva o resultado
void print_result(FILE* f_out, char* op);

// Pega o posicao de um id na memoria
int id_pos(FILE* f_out, char* id);

// Verifica se um id eh uma arranjo
int is_array(char* ch);

// Retorno o tamanho gasto pelas variaveis
int get_var_space(FILE* f_in);

// Retorna o tamanho de um id
int id_size(char* id);

#endif
