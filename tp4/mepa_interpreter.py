import sys

# Operacoes aritmeticas
arith_2_op = {"SOMA": lambda x, y: x + y, "SUBT": lambda x, y: x - y, 
              "MULT": lambda x, y: x * y, "DIVI": lambda x, y: x / y,
              "CMMA": lambda x, y: x > y, "CMME": lambda x, y: x < y,
              "CMEG": lambda x, y: x <= y, "CMAG": lambda x, y: x >= y,
              "CMIG": lambda x, y: x == y, "CMDG": lambda x, y: x != y,
              "DISJ": lambda x, y: x | y, "CONJ": lambda x, y: x & y}

arith_1_op = {"INVR": lambda x: -x, "NEGA": lambda x: not x}


# Le o arquivo de entrar
def read_file(input_filepath):
    
    with open(input_filepath, "r") as f:
        lines = f.readlines()

    # Retira os labels
    labels = {}
    for i, line in enumerate(lines):
        if ":" in line:
            l = line.split(":")[0]
            lines[i] = line.split(":")[1]
            labels[l] = i

    # Remove \n no final
    lines = [x.strip() for x in lines]
    
    return lines, labels

# Executa o programa MEPA
def execute_program(commands, labels):
    
    if commands[0] != "INPP":
        print("Erro, falta o comando de inicio do programa.")
        return;
    
    print("Iniciou programa")
    # Instruction pointer
    ip = 0
    
    # Stack
    stack = []
    sp = 0
    
    while (ip < len(commands)):
        # Linha vaiza
        if len(commands[ip]) == 0:
            ip += 1
            continue
        
        # Pega o instrucao
        inst = commands[ip].split(" ")[0]

        # Fim do programa
        if inst == "PARA":
            print("Finalizou programa")
            break;
        
        # Carrega constante
        if inst == "CRCT":
            ct = commands[ip].split(" ")[1]
            if "." in ct:
                ct = float(ct)
            else:
                ct = int(ct)
                
            stack.append(ct)
            #print("Carregou a constante: %s" % str(ct))
        
        elif inst == "DSVS":
            l = commands[ip].split(" ")[1]
            ip = labels[l] - 1
            print("Desvio para %s" % l)
            
        elif inst == "DSVF":
            if stack[-1] is False:
                l = commands[ip].split(" ")[1]
                ip = labels[l] - 1
                print("Desvio para %s" % l)
            stack = stack[:-1]
        
        elif inst == "AMEM":
            n = int(commands[ip].split(" ")[1])
            stack = stack[:sp] + [0]*n + stack[sp:]
            sp += n
            #print("Alocou posicoes: %d" % n)

        elif inst == "CRVL":
            m = int(commands[ip].split(" ")[2])
            stack.append(stack[m])
            #print("Carregou topo da posicao %d" % m)
        
        elif inst == "ARMZ":
            m = int(commands[ip].split(" ")[2])
            stack[m] = stack[-1]
            stack = stack[:-1]
            #print("Salvou topo da posicao %d" % m)
        
        elif inst == "CRVP":
            m = int(commands[ip].split(" ")[2]) + stack[-1]
            stack[-1] = stack[m]
            #print("Carregou topo da posicao %d" % m)
        
        elif inst == "ARMP":
            m = int(commands[ip].split(" ")[2]) + stack[-1]
            stack[m] = stack[-2]
            stack = stack[:-2]
            #print("Salvou topo da posicao %d" % m)

        # Operacao artimetica de 2 operadores
        elif inst in arith_2_op.keys():
            result = arith_2_op[inst](stack[-2], stack[-1])
            stack = stack[:-2]
            stack.append(result)
            
            print("Resultado do %s: %s" % (inst, str(result)))
        # Operacao artimetica de 1 operador
        elif inst in arith_1_op.keys():
            result = arith_1_op[inst](stack[-1])
            stack = stack[:-1]
            stack.append(result)
            
            print("Resultado do %s: %s" % (inst, str(result)))
        
        ip += 1

if __name__ == "__main__":
    input_filepath = sys.argv[1]

    commands, labels = read_file(input_filepath)

    execute_program(commands, labels)

