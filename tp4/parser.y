%{

#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>
using namespace std;

void yyerror(char *s);
int yylex(void);
void new_temp(char* temp);
void new_label(char* label);
void backpatch(char* code, char* label, char placeholder);

%}

%union {
  int int_num;
  float real_num;
  char* str;

  struct {
    char code[1000];
    char addr[100];
  } expr_type;

  struct {
    char code[1000];
    char if_true[100];
    char if_false[100];
    char next[100];
    char begin[100];
    int lbl_flag;
    int break_flag;
  } stmt_type;
};

%token SEMICOLON EQUALS LOGICAL_OR LOGICAL_AND IS_EQUAL_TO IS_NOT_EQUAL_TO
%token LESS_THAN GREATER_THAN LESS_EQUAL_THAN GREATER_EQUAL_THAN
%token MATH_PLUS MATH_MINUS MATH_TIMES MATH_DIVIDE LOGICAL_NOT
%token BRACKET_BEGIN BRACKET_END PARENTHESES_BEGIN PARENTHESES_END BLOCK_BEGIN BLOCK_END
%token INTEGER_TYPE BOOLEAN_TYPE BOOLEAN_TRUE BOOLEAN_FALSE REAL_TYPE
%token IF ELSE WHILE DO BREAK ID_VARIABLE

%token <int_num> INTEGER_NUM
%token <real_num> FLOAT_NUM

%nonassoc NO_ELSE
%nonassoc ELSE

%type <expr_type> factor
%type <expr_type> loc
%type <expr_type> bool
%type <expr_type> unary
%type <expr_type> term
%type <expr_type> expr
%type <expr_type> rel
%type <expr_type> equality
%type <expr_type> join
%type <stmt_type> stmt
%type <stmt_type> stmts
%type <stmt_type> block

%%
program: block {printf("%s\n", $1.code);}
    | block error
  ;

block:
    BLOCK_BEGIN decls stmts BLOCK_END {$$.break_flag = $3.break_flag; if (strlen($3.next) > 0) sprintf($$.code, "%s%s: ", $3.code, $3.next); else strcpy($$.code, $3.code);}
  ;

decls:
  | decls decl
  ;

decl:
    type ID_VARIABLE SEMICOLON
  ;

type:
    type BRACKET_BEGIN INTEGER_NUM BRACKET_END
  | INTEGER_TYPE
  | BOOLEAN_TYPE
  | REAL_TYPE
  ;

stmts:         {strcpy($$.code, ""); strcpy($$.next, "");}
  | stmts stmt {strcpy($$.next, $2.next); $$.break_flag = $1.break_flag + $2.break_flag; if ($2.lbl_flag || (strlen($1.next) != 0)) { if(strlen($1.next) == 0) new_label($1.next); backpatch($2.code, $1.next, '_');
                                                             sprintf($$.code, "%s%s: %s", $1.code, $1.next, $2.code); }
                                          else  sprintf($$.code, "%s%s", $1.code, $2.code); }
  ;

stmt:
    loc EQUALS bool SEMICOLON                                      {$$.lbl_flag = 0; $$.break_flag = 0; sprintf($$.code, "%s%s%s = %s\n", $$.code, $3.code, $1.addr, $3.addr); strcpy($$.next, "");}
  | IF PARENTHESES_BEGIN bool PARENTHESES_END stmt %prec NO_ELSE   {$$.lbl_flag = 0; $$.break_flag = $5.break_flag; new_label($$.next);
                                                                    sprintf($$.code, "%siffalse %s goto %s\n%s", $3.code, $3.addr, $$.next, $5.code);}

  | IF PARENTHESES_BEGIN bool PARENTHESES_END stmt ELSE stmt       {$$.lbl_flag = 0; $$.break_flag = $5.break_flag + $7.break_flag; new_label($$.if_true); new_label($$.next);
                                                                    sprintf($$.code, "%sif %s goto %s\n%sgoto %s\n%s: %s", $3.code, $3.addr, $$.if_true, $7.code, $$.next, $$.if_true, $5.code);}

  | WHILE PARENTHESES_BEGIN bool PARENTHESES_END stmt              {$$.lbl_flag = 1; $$.break_flag = 0; new_label($$.next); backpatch($5.code, $$.next, '@');
                                                                    sprintf($$.code, "%siffalse %s goto %s\n%sgoto _\n", $3.code, $3.addr, $$.next, $5.code);}

  | DO stmt WHILE PARENTHESES_BEGIN bool PARENTHESES_END SEMICOLON {$$.lbl_flag = 1; if($2.break_flag) {new_label($$.next); backpatch($2.code, $$.next, '@'); }
                                                                    sprintf($$.code, "%s%sif %s goto _\n", $2.code, $5.code, $5.addr); $$.break_flag = 0;}

  | BREAK SEMICOLON                                                {$$.break_flag = 1; strcpy($$.code, "goto @\n"); strcpy($$.next, "");}

  | block                                                          {strcpy($$.code, $1.code); strcpy($$.next, $1.next); $$.lbl_flag = 0; $$.break_flag = $1.break_flag;}
  ;

bool:
    bool LOGICAL_OR join {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s || %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | join                 {strcpy($$.addr, $1.addr); strcpy($$.code, $1.code); }
  ;

join:
    join LOGICAL_AND equality {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s && %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | equality                  {strcpy($$.addr, $1.addr); strcpy($$.code, $1.code); }
  ;

equality:
    equality IS_EQUAL_TO rel     {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s == %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | equality IS_NOT_EQUAL_TO rel {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s != %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | rel                          {strcpy($$.addr, $1.addr); strcpy($$.code, $1.code); }
  ;

rel:
    expr LESS_THAN expr          {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s < %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | expr LESS_EQUAL_THAN expr    {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s <= %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | expr GREATER_EQUAL_THAN expr {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s >= %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | expr GREATER_THAN expr       {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s > %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | expr                         {strcpy($$.addr, $1.addr); strcpy($$.code, $1.code); }
  ;

expr:
    expr MATH_PLUS term   {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s + %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | expr MATH_MINUS term  {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s - %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | term                  {strcpy($$.addr, $1.addr); strcpy($$.code, $1.code); }
  ;

term:
    term MATH_TIMES unary  {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s * %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | term MATH_DIVIDE unary {new_temp($$.addr); sprintf($$.code, "%s%s%s = %s / %s\n", $1.code, $3.code, $$.addr, $1.addr, $3.addr);}
  | unary                  {strcpy($$.addr, $1.addr); strcpy($$.code, $1.code); }
  ;

unary:
    LOGICAL_NOT unary {new_temp($$.addr); sprintf($$.code, "%s = ! %s\n", $$.addr, $2.addr); }
  | MATH_MINUS unary  {new_temp($$.addr); sprintf($$.code, "%s = - %s\n", $$.addr, $2.addr); }
  | factor            {strcpy($$.addr, $1.addr); strcpy($$.code, $1.code);}
  ;

factor:
    PARENTHESES_BEGIN bool PARENTHESES_END {strcpy($$.addr, $2.addr); strcpy($$.code, $2.code);}
  | loc                                    {strcpy($$.addr, $1.addr); strcpy($$.code, $1.code);}
  | INTEGER_NUM                            {sprintf($$.addr, "%d", yyval.int_num); strcpy($$.code, "");}
  | FLOAT_NUM                              {sprintf($$.addr, "%f", yyval.real_num); strcpy($$.code, "");}
  | BOOLEAN_TRUE                           {strcpy($$.addr, "true"); strcpy($$.code, "");}
  | BOOLEAN_FALSE                          {strcpy($$.addr, "false"); strcpy($$.code, "");}
  ;

  loc:
    loc BRACKET_BEGIN bool BRACKET_END {char i[100]; new_temp(i); sprintf($$.addr, "%s[%s]", $1.addr, i); sprintf($$.code, "%s%s = %s * 8\n", $3.code, i, $3.addr);}
  | ID_VARIABLE                        {strcpy($$.addr, yyval.str); strcpy($$.code, "");}
  ;

%%
void yyerror(string s) {
  extern int yylineno;	// defined in %option yylineno at flex file
  extern char *yytext;	// defined and maintained at flex file

  cerr << "P_ERROR: " << s << " at symbol \"" << yytext;
  cerr << "\" on line " << yylineno << endl;
}

void yyerror(char *s) {
  return yyerror(string(s));
}

// Gera um novo temporario
int next_temp = 1;
void new_temp(char* temp) {
  sprintf(temp, "t%d", next_temp);

  next_temp += 1;
}

// Gera um novo label
int next_label = 1;
void new_label(char* label) {
  sprintf(label, "L%d", next_label);

  next_label += 1;
}

// Adiciona o destino do goto no codigo gerado
void backpatch(char* code, char* label, char placeholder) {
  int i, j, k, len, len_label;
  char temp[1000];

  len = strlen(code);
  len_label = strlen(label);

  for(i=0, j=0; i<len; i++) {
    if(code[i] == placeholder)  {
      for(k=0; k<len_label; k++) {
        temp[j] = label[k];
        j++;
      }
    } else {
      temp[j] = code[i];
      j++;
    }
  }
  temp[j] = '\0';

  strcpy(code, temp);
}
