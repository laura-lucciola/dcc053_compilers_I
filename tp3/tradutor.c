#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// #define DEBUG

#define ARRAY_SIZE 100

// Estrutura que salva a posicao dos nomes na memoria
typedef struct {
	char name[100];
	int offset;
} id;

id enderecos[100];
int end_len;
int offset;

// Traduz uma linha de codigo intermediario
void translate_line(char* line);

// Verifica se o operando eh um numero
int is_number(char* ch);

// Imprime o codigo que empilha o operando
void imprime_operando(char* op);

// Imprime o codigo de um operador unario
void print_operator1(char* op);

// Imprime o codigo de um operador bianrio
void print_operator2(char* op);

// Imprime o codigo que salva o resultado
void print_result(char* op);

// Pega o posicao de um id na memoria
int id_pos(char* id);

// Verifica se um id eh uma arranjo
int is_array(char* ch);

int main(int argc, char* argv[]) {
	// Checa numero de argumentos
	if(argc != 2) {
		printf("Erro, numero incorreto de parametros");
		return 0;
	}

	// Arquivo de entrada
	FILE* f_in;
	f_in = fopen(argv[1], "r");

	// Verifica se o arquivo foi lido
	if (f_in == NULL) {
		printf("Erro ao abrir arquivo(s)");
		return 0;
	}

	// Le o arquivo de entrada
	char* line = NULL;
	size_t len = 0;
    ssize_t read;

    // Inicializa a posicao dos enderecos
 	end_len = 0;
 	offset = 0;

    printf("INPP\n");

    // Traduz o codigo linha por linha
	while ((read = getline(&line, &len, f_in)) != -1) {
        translate_line(line);
    }

	printf("PARA\n");

    fclose(f_in);

	return 0;
}

void translate_line(char* line) {
	char* ch;

	// Modo de debug que imprime o codigo intermediario tambem
	#ifdef DEBUG
	printf("\n");
	printf("// %s", line);
	if(line[strlen(line)-1] != '\n')
		printf("\n");
	#endif

	// Remove \n no final se houver
	if(line[strlen(line)-1] == '\n')
		line[strlen(line)-1] = '\0';

	// Se for uma linha vazia
	if(strlen(line) == 0)
		return;

	// Le o primeiro token
	ch = strtok(line, " ");

	// Enquanto for um rotulo
	while(ch[strlen(ch)-1] == ':') {
		printf("%s ", ch);
		ch = strtok(NULL, " ");
		if(ch == NULL) {
			printf("\n");
			return;
		}
	}

	// Goto
	if(strcmp(ch, "goto") == 0) {
		ch = strtok(NULL, " ");
		printf("DSVS %s\n", ch);
		return;
	}

	// Iffalse
	if(strcmp(ch, "iffalse") == 0) {
		char* cond;

		// Condicao do if
		cond = strtok(NULL, " ");
		imprime_operando(cond);

		// Pula o goto
		strtok(NULL, " ");

		// Destino do goto
		ch = strtok(NULL, " ");

		printf("DSVF %s \n", ch);
		return;
	}

	// If
	if(strcmp(ch, "if") == 0) {
		char* cond;

		// Condicao do if
		cond = strtok(NULL, " ");
		imprime_operando(cond);

		// Nega a condicao
		printf("NEGA\n");

		// Pula o goto
		strtok(NULL, " ");

		// Destino do goto
		ch = strtok(NULL, " ");

		printf("DSVF %s \n", ch);
		return;
	}

	// Atribuicoes
	char *result, *fst_op, *snd_op, *op;
	result = ch;

	// Pula o =
	strtok(NULL, " ");

	// Primeiro operando
	fst_op = strtok(NULL, " ");
	imprime_operando(fst_op);

	// Operador
	op = strtok(NULL, " ");

	// Se o operando for nulo, faz apenas uma atribuicao direta
	if(op == NULL) {
		print_result(result);
		return;
	}

	// Se tiver segundo operando
	snd_op = strtok(NULL, " ");
	if(snd_op != NULL) {
		imprime_operando(snd_op);
		print_operator2(op);

	} else { // Se nao imprime o operador unario
		print_operator1(op);
	}

	// Salva resultado
	print_result(result);
}

int id_pos(char* id) {
	int i, j;

	// Se nao for array
	if(is_array(id) == 0) {
		// Se a entrada ja existir
		for(i=0; i<end_len; i++) {
			if(strcmp(enderecos[i].name, id) == 0) {
				return enderecos[i].offset;
			}
		}

		// Senao cria uma nova
		printf("AMEM 1\n");
		enderecos[i].offset = offset;
		strcpy(enderecos[i].name, id);

		offset += 1;
		end_len += 1;
	} else {
		char arr[100], ind[100];

		// Separa o nome do array e o indice
		i = 0;
		while(id[i] != '[') {
			arr[i] = id[i];
			i += 1;
		}
		arr[i] = '\0';

		i += 1;
		j = 0;
		while(id[i] != ']') {
			ind[j] = id[i];
			j += 1;
			i += 1;
		}
		ind[j] = '\0';

		// Se a entrada ja existir
		for(i=0; i<end_len; i++) {
			if(strcmp(enderecos[i].name, arr) == 0) {
				return enderecos[i].offset;
			}
		}

		// Senao cria uma nova
		printf("AMEM %d\n", ARRAY_SIZE);
		enderecos[i].offset = offset;
		strcpy(enderecos[i].name, arr);

		offset += ARRAY_SIZE;
		end_len += 1;

	}

	return enderecos[i].offset;
}

int is_array(char* ch) {
	if(ch[strlen(ch)-1] == ']')
		return 1;
	else
		return 0;
}


int is_number(char* ch) {
	int i;
	for(i=0; i<strlen(ch); i++) {
		if(isdigit(ch[i]) == 0 && ch[i] != '.') {
			return 0;
		}
	}

	return 1;
}

void print_result(char* op) {
	int pos, i, j;
	pos = id_pos(op);

	// Verifica se eh arranjo
	if(is_array(op) == 0)
		printf("ARMZ 0 %d\n", pos);
	else {
		char arr[100], ind[100];

		// Separa o nome do array e o indice
		i = 0;
		while(op[i] != '[') {
			arr[i] = op[i];
			i += 1;
		}
		arr[i] = '\0';

		i += 1;
		j = 0;
		while(op[i] != ']') {
			ind[j] = op[i];
			j += 1;
			i += 1;
		}
		ind[j] = '\0';

		// Carrega o indice na pilha
		imprime_operando(ind);

		printf("ARMP 0 %d\n", pos);
	}
}

void imprime_operando(char* op) {
	// Constante false
	if(strcmp(op, "false") == 0) {
		printf("CRCT 0\n");
		return;
	}

	// Constante true
	if(strcmp(op, "true") == 0) {
		printf("CRCT 1\n");
		return;
	}

	// Constante numerica
	if(is_number(op)) {
		printf("CRCT %s\n", op);
		return;
	}

	// Ids
	int pos, i, j;
	pos = id_pos(op);
	if(is_array(op) == 0)
		printf("CRVL 0 %d\n", pos);
	else {
		char arr[100], ind[100];

		// Separa o nome do array e o indice
		i = 0;
		while(op[i] != '[') {
			arr[i] = op[i];
			i += 1;
		}
		arr[i] = '\0';

		i += 1;
		j = 0;
		while(op[i] != ']') {
			ind[j] = op[i];
			j += 1;
			i += 1;
		}
		ind[j] = '\0';

		// Carrega o indice na pilha
		imprime_operando(ind);

		printf("CRVP 0 %d\n", pos);
	}
}

void print_operator1(char* op) {
	if(strcmp(op, "-") == 0) {
		printf("INVR\n");
		return;
	}

	if(strcmp(op, "!") == 0) {
		printf("NEGA\n");
		return;
	}
}

void print_operator2(char* op) {
	if(strcmp(op, "+") == 0) {
		printf("SOMA\n");
		return;
	}

	if(strcmp(op, "-") == 0) {
		printf("SUBT\n");
		return;
	}

	if(strcmp(op, "*") == 0) {
		printf("MULT\n");
		return;
	}

	if(strcmp(op, "/") == 0) {
		printf("DIVI\n");
		return;
	}

	if(strcmp(op, ">") == 0) {
		printf("CMMA\n");
		return;
	}

	if(strcmp(op, "<") == 0) {
		printf("CMME\n");
		return;
	}

	if(strcmp(op, "<=") == 0) {
		printf("CMEG\n");
		return;
	}

	if(strcmp(op, ">=") == 0) {
		printf("CMAG\n");
		return;
	}

	if(strcmp(op, "==") == 0) {
		printf("CMIG\n");
		return;
	}

	if(strcmp(op, "!=") == 0) {
		printf("CMDG\n");
		return;
	}

	if(strcmp(op, "||") == 0) {
		printf("DISJ\n");
		return;
	}

	if(strcmp(op, "&&") == 0) {
		printf("CONJ\n");
		return;
	}
}
